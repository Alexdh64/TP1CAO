﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1CAO
{
    //La classe Paquet est la classe mere des differentes entites qui vont gerer les interactions entre les paquets de cartes constituant le jeu
    class Paquet
    {
        private Carte[] paquet;
        private int quantite;

        public Paquet(int quantite)
        {
            this.quantite = quantite;
            paquet = new Carte[quantite];
        }

        public int Quantite
        {
            get { return quantite; }
            set { quantite = value; }
        }

        public Carte this[int index]
        {
            get { return paquet[index]; }
            set { paquet[index] = value; }
        }

        public bool estVide()
        {
            if (quantite == 0)
                return true;
            else
                return false;
        }
    }
}
