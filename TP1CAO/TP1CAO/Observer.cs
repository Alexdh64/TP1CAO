﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1CAO
{
    class Observer
    {
        public Observer()
        {

        }

        //Méthode qui affiche le nom du joueur et la carte qu'il a jouée ou piochée.
        public void update(Joueur joueur, Carte carte, bool joue)
        {
            if(joue)
                Console.WriteLine("{0} a joue la carte {1}.", joueur.Nom, carte.Nom);
            else
                Console.WriteLine("{0} a piochee la carte {1}.", joueur.Nom, carte.Nom);
        }
    }
}
