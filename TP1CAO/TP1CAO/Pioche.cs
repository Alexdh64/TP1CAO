﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1CAO
{
    //Paquet d'ou les joueurs vont piocher leurs cartes
    class Pioche : Paquet
    {
        private Carte[] pioche;

        public Pioche(int quantite = 52) : base(quantite)
        {
            pioche = new Carte[quantite];
        }

        //Méthode pour generer au hasard le paquet de pioche au debut de la partie
        public void genererPaquet()
        {
            Random rnd = new Random();
            int index;
            for (int v = 1; v <= 13; v++)
            {
                for (int c = 1; c <= 4; c++)
                {
                    Carte carte = new Carte((Valeur)v, (Couleur)c);
                    index = rnd.Next(0, this.Quantite - 1);
                    //Si l'emplacement est deja occupe, on incremente l'index jusqu'a ce qu'on trouve un emplacement libre.
                    while (pioche[index] != null)
                    {
                        if (index == this.Quantite - 1)
                            index = 0;
                        else
                            index++;
                    }
                    pioche[index] = carte;

                }
            }
        }

        //Piger une carte du paquet de pioche. On reduit la taille du paquet de 1.
        public Carte prendreCarte()
        {
            Carte carteAPiocher = pioche[this.Quantite-- - 1];
            pioche[this.Quantite] = null;
            Carte[] temp = new Carte[this.Quantite];
            for (int i = 0; i < this.Quantite; i++)
            {
                temp[i] = pioche[i];
            }
            pioche = temp;
            return carteAPiocher;
        }

        //Si la partie se rend jusqu'a la fin du paquet de pioche, on melange les carte du paquet de depot puis on en fait le nouveau paquet de pioche. 
        public void absorberDepot(Carte[] depot)
        {
            int index;
            Random rnd = new Random();
            this.Quantite = depot.Length;
            Carte[] temp = new Carte[this.Quantite];
            pioche = temp;
            for (int i = 0; i < this.Quantite; i++)
            {
                index = rnd.Next(0, this.Quantite - 1);
                while (pioche[index] != null)
                {
                    if (index == this.Quantite - 1)
                        index = 0;
                    else
                        index++;
                }
                pioche[index] = depot[i];
            }
        }
    }
}
