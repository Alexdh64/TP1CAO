﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1CAO
{
    //Ces deux types enums representent les valeurs possible de deux des attributs de la classe Carte
    enum Valeur { As = 1, Deux, Trois, Quatre, Cinq, Six, Sept, Huit, Neuf, Dix, Valet, Reine, Roi }
    enum Couleur { Pique = 1, Trefle, Coeur, Carreaux }

    //La classe Carte est l'entite au centre de notre jeu. Elle comprend une Valeur, une Couleur et un nom qui est determine par la valeur et la couleur
    class Carte : IComparable
    {
        private Valeur valeur;
        private Couleur couleur;
        private string nom;

        public Carte(Valeur valeur, Couleur couleur)
        {
            this.valeur = valeur;
            this.couleur = couleur;
            nom = String.Format("{0} de {1}", valeur, couleur);
        }

        //Dans le cadre du jeu, deux cartes sont considerees equivalentes si leur couleur OU leur valeur sont equivalentes
        public int CompareTo(Object obj)
        {
            Carte carte = (Carte)obj;
            if (carte.couleur == this.couleur || carte.valeur == this.valeur)
                return 1;
            else
                return 0;
        }

        public string Nom
        {
            get { return nom; }
        }


    }
}
