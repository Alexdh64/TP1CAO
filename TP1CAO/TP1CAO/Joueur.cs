﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1CAO
{
    //Conceptuellement, la classe Joueur est un paquet avec un nom et des facon d'interagir avec d'autres paquets
    class Joueur : Paquet
    {
        private Carte[] jeu;
        private Carte carteAJouer;
        private int position;
        private string nom;
        Observer observer = new Observer();

        public Joueur(string nom, int quantite = 0) : base(quantite)
        {
            this.nom = nom;
            jeu = new Carte[quantite];
        }

        //Getter et setteur utilises dans le main et dans la classe observable
        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        public Carte[] Jeu
        {
            get { return jeu; }
        }

        //Fonction pour afficher la main du joueur
        public String voirMain()
        {
            string stringCartes = "";
            foreach (Carte carte in jeu)
            {
                stringCartes += carte.Nom + " \n";
            }
            return stringCartes;
        }

        //On compare le paquet du joueur avec la pile de depot pour determiner si une carte est compatible
        public bool jouable(Carte carteDepot)
        {
            for (int i = 0; i < this.Quantite; i++)
            {
                if (jeu[i].CompareTo(carteDepot) == 1)
                {
                    position = i;
                    return true;
                }
            }
            return false;
        }

        //On enleve la carte à jouer et on reduit la taille du paquet du joueur. La fonction retourne la carte jouee. Par defaut, la premiere carte est jouee
        public Carte jouerCarte(int index = 0)
        {
            if (position != 0)
                index = position;
            carteAJouer = jeu[index];
            Carte[] temp = new Carte[this.Quantite - 1];
            int j = 0;
            for (int i = 0; i < this.Quantite; i++)
            {
                if (i != index)
                    temp[j++] = jeu[i];
            }
            jeu = temp;
            this.Quantite--;

            observer.update(this, carteAJouer, true);

            return carteAJouer;
        }

        //On grandit le paquet de 1, puis on ajoute la carte passee en parametre au paquet
        public void piocherCarte(Carte cartePioche)
        {
            Carte[] temp = new Carte[this.Quantite + 1];
            for (int i = 0; i < this.Quantite; i++)
            {
                temp[i] = jeu[i];
            }
            temp[this.Quantite++] = cartePioche;
            jeu = temp;

            observer.update(this, cartePioche, false);
        }

        //Fonction pour acceder et modifier directement les cartes d'un joueur
        public Carte this[int index]
        {
            get { return jeu[index]; }
            set { jeu[index] = value; }
        }

    }
}
