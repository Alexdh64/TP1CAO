﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1CAO
{
    //Paquet ou les joueurs deposent leurs cartes.
    class Depot : Paquet
    {
        private Carte[] depot;

        public Depot(int quantite = 0) : base(quantite)
        {
            depot = new Carte[quantite];
        }

        //On ajoute une carte au paquet de depot. On l'agrandi de 1.
        public void ajouterCarte(Carte carte)
        {
            Carte[] temp = new Carte[this.Quantite + 1];
            for (int i = 0; i < this.Quantite; i++)
            {
                temp[i] = depot[i];
            }
            temp[this.Quantite++] = carte;
            depot = temp;
        }

        //On affiche la carte au sommet de la pile du depot, dans notre cas la carte que les joueurs devront jouer dessus.
        public Carte montrerCarte()
        {
            return depot[this.Quantite - 1];
        }

        //On transfere le paquet de depot au paquet de pioche. On garde une carte dans le paquet depot pour que le jeu puisse continuer
        public Carte[] transferAPioche()
        {
            Carte[] transfer = new Carte[this.Quantite - 1];
            int j = this.Quantite - 2;
            for (int i = this.Quantite - 1; i > 0; i--)
            {
                transfer[j--] = depot[i];
            }
            Carte[] temp = new Carte[1];
            temp[0] = depot[0];
            depot = temp;
            this.Quantite = 1;
            return transfer;
        }
    }
}
