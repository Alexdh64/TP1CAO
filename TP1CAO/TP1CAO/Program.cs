﻿using System;
using System.Threading;

namespace TP1CAO
{   
    class Program
    {
        static void Main(string[] args)
        {          
            Console.WriteLine("************************************************************************************************************************");
            Console.WriteLine("                                           Bienvenue au jeu de Peche Pioche!                                            ");
            Console.WriteLine("************************************************************************************************************************");

            //Creation des joueurs
            Console.WriteLine("\nVeuillez s'il vous plait entrer le nombre de participants (2, 3 ou 4).");
            string userInput;
            do
            {
                userInput = Console.ReadLine();
                if (userInput != "2" && userInput != "3" && userInput != "4")
                    Console.WriteLine("Vous devez entrer le nombre 2, 3 ou 4 pour specifier le nombre de participants!");
            } while (userInput != "2" && userInput != "3" && userInput != "4");
            Joueur[] joueur = new Joueur[Convert.ToInt16(userInput)];
            for(int i = 0; i < joueur.Length; i++)
            {
                Console.WriteLine("Veuillez entrer le nom du joueur {0}", i + 1);
                joueur[i] = new Joueur(Console.ReadLine());
            }

            //Option de vitesse de la partie
            Console.WriteLine("\nSi vous voulez que la partie se deroule le plus rapidement possible, entrez 1. Sinon, entrez n'importe quoi d'autre.");
            bool instant = false;
            if (Console.ReadLine() == "1")
                instant = true;

            //Creation du paquet de pioche et distribution des cartes
            Console.WriteLine("\n8 cartes sont distribuees a chaques joueurs.");
            Pioche pioche = new Pioche();
            pioche.genererPaquet();
            for (int i = 0; i < joueur.Length; i++)
            {
                for (int j = 0; j < 8; j++)
                    joueur[i].piocherCarte(pioche.prendreCarte());
                Console.WriteLine("\nMain de depart de {0}:\n{1}", joueur[i].Nom, joueur[i].voirMain());
                if(!instant)
                    Thread.Sleep(2000);
            }

            //Determination du premier tour du jeu
            Depot depot = new Depot();
            Random rnd = new Random();
            int tour = rnd.Next(0, joueur.Length - 1);
            Console.WriteLine("\n{0} depose la carte {1} sur le jeu pour le commencer.", joueur[tour].Nom, joueur[tour][0].Nom);
            depot.ajouterCarte(joueur[tour].jouerCarte());

            //Jeu!!
            bool gagnant = false;
            int compteur = 0;
            do
            {
                Console.WriteLine("\n\n------------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine("Tour {0}", ++compteur);
                Console.WriteLine("\n------------------------------------------------------------------------------------------------------------------------");
                if (!instant)
                    Thread.Sleep(750);
                if (tour == joueur.Length - 1)
                    tour = 0;
                else
                    tour++;
                Console.WriteLine("\nC'est a {0} de jouer.", joueur[tour].Nom);
                if (!instant)
                    Thread.Sleep(750);
                Console.WriteLine("\nCarte sur la pile de depot: \n{0}", depot.montrerCarte().Nom);
                if (!instant)
                    Thread.Sleep(1000);
                Console.WriteLine("\nCartes dans la main de {0}:", joueur[tour].Nom);
                if (!instant)
                    Thread.Sleep(400);
                Console.WriteLine("{0}", joueur[tour].voirMain());
                if (!instant)
                    Thread.Sleep(2500);
                if (joueur[tour].jouable(depot.montrerCarte()))
                {
                    Carte carteJoue;
                    depot.ajouterCarte(carteJoue = joueur[tour].jouerCarte());
                    if (!instant)
                        Thread.Sleep(500);
                    if (joueur[tour].estVide())
                        gagnant = true;
                }
                else
                {
                    Carte cartePiochee;
                    joueur[tour].piocherCarte(cartePiochee = pioche.prendreCarte());
                    if (!instant)
                        Thread.Sleep(500);
                }                
                if (pioche.estVide())
                {
                    Console.WriteLine("\n************************************************************************************************************************");
                    Console.WriteLine("                                 Brasse du paquet depot pour devenir le paquet pioche...");
                    Console.WriteLine("\n************************************************************************************************************************");
                    pioche.absorberDepot(depot.transferAPioche());
                    if (!instant)
                        Thread.Sleep(500);
                }
                if (!instant)
                    Thread.Sleep(1000);
            } while (!gagnant);
            Console.WriteLine("\n************************************************************************************************************************");
            Console.WriteLine("                                         {0} a gagne la partie! Felicitation!", joueur[tour].Nom);
            Console.WriteLine("\n************************************************************************************************************************");
            Console.ReadKey(true); 


        }
    }
}
